#!/bin/bash

[ -z $HOST ] && echo "No host provided!" && exit 1

rsync -rzf '-! index.html' --chmod=a+r --info=name ./ $HOST:html